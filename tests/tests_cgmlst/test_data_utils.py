import hashlib

import pandas as pd
import pytest

from coreprofiler.cgmlst.data_utils import (
    hash_to_numerical,
)


###################################
# --- hash_to_numerical tests --- #
###################################


@pytest.fixture
def hash_to_numerical_entries():
    hash1 = f">locus1_{hashlib.md5(('A' * 37).encode()).hexdigest()}"
    hash2 = f">locus1_{hashlib.md5(('AT' * 157).encode()).hexdigest()}"
    hash3 = f">locus2_{hashlib.md5(('ATCG').encode()).hexdigest()}"
    hash4 = f">locus2_{hashlib.md5(('GC' * 3500).encode()).hexdigest()}"
    hash5 = f">locus3_{hashlib.md5(('TC' * 28).encode()).hexdigest()}"
    allele_calling_df = {
        "locus1": [1, 2, hash1, hash2],
        "locus2": [hash3, 1, hash4, hash3],
        "locus3": [7, hash5, 2, 2],
    }
    allele_calling_df = pd.DataFrame(
        allele_calling_df,
        index=["isolate1", "isolate2", "isolate3", "isolate4"],
    )

    new_alleles_in_profiles = {
        "locus1": ["isolate3", "isolate4", "isolate5"],
        "locus2": ["isolate1", "isolate3", "isolate4"],
        "locus3": ["isolate2", "isolate6"],
    }

    number_alleles = {
        "locus1": 10,
        "locus2": 20,
        "locus3": 30,
        "locus4": 40,
        "locus5": 50,
    }
    return allele_calling_df, new_alleles_in_profiles, number_alleles


def test_hash_to_numerical_valid(hash_to_numerical_entries):
    output = hash_to_numerical(*hash_to_numerical_entries)
    profiles_numerical_data = {
        "locus1": [1, 2, 11, 12],
        "locus2": [21, 1, 22, 21],
        "locus3": [7, 31, 2, 2],
    }
    profiles_numerical_df = pd.DataFrame(
        profiles_numerical_data,
        index=["isolate1", "isolate2", "isolate3", "isolate4"],
    )
    output = output.astype("int64")
    profiles_numerical_df = profiles_numerical_df.astype("int64")
    assert output.equals(profiles_numerical_df)


@pytest.mark.exception
def test_hash_to_numerical_non_hash(hash_to_numerical_entries):
    allele_calling_df = hash_to_numerical_entries[0]
    allele_calling_df.loc["isolate3", "locus1"] = 3
    with pytest.raises(ValueError):
        hash_to_numerical(allele_calling_df, *hash_to_numerical_entries[1:])
