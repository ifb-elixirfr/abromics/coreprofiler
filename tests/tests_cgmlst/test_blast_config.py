import pytest
from coreprofiler.cgmlst.blast_config import BlastConfig
from pathlib import Path


valid_params = {
    "blast_type": "blastn",
    "query_path": Path("tests/data/genomes/10_04A025_hennart2022.fas"),
    "num_threads": 4,
    "perc_identity": 90,
    "max_target_seqs": 10,
    "ungapped": False,
    "outfmt": [
        "6",
        "evalue",
        "bitscore",
        "pident",
        "qseqid",
        "slen",
        "length",
        "nident",
        "qseqid",
        "qstart",
        "qend",
        "sstrand",
    ],
    "db_path": Path(
        "tests/data/kp_scheme_sample_db/" + "kp_scheme_sample_db.fasta"
    ),
    "subject": None,
    "word_size": 31,
    "task": "blastn",
}


def test_valid_blast_config():
    blast_config = BlastConfig(**valid_params)
    for param, value in valid_params.items():
        assert getattr(blast_config, param) == value


@pytest.mark.exception
def test_invalid_query_path():
    invalid_params = valid_params.copy()
    invalid_params["query_path"] = Path("invalid.fasta")
    with pytest.raises(FileNotFoundError, match="Invalid query file path"):
        BlastConfig(**invalid_params)


@pytest.mark.exception
def test_invalid_blast_type():
    invalid_params = valid_params.copy()
    invalid_params["blast_type"] = "invalid"
    with pytest.raises(ValueError, match="Invalid BLAST type"):
        BlastConfig(**invalid_params)


@pytest.mark.exception
def test_invalid_num_threads():
    invalid_params = valid_params.copy()
    invalid_params["num_threads"] = -1
    with pytest.raises(ValueError, match="Invalid number of threads"):
        BlastConfig(**invalid_params)


@pytest.mark.exception
def test_invalid_perc_identity():
    invalid_params = valid_params.copy()
    invalid_params["perc_identity"] = 101
    with pytest.raises(ValueError, match="Invalid percent identity"):
        BlastConfig(**invalid_params)


@pytest.mark.exception
def test_invalid_max_target_seqs():
    invalid_params = valid_params.copy()
    invalid_params["max_target_seqs"] = -1
    with pytest.raises(
        ValueError, match="Invalid maximum number of target sequences"
    ):
        BlastConfig(**invalid_params)


@pytest.mark.exception
def test_invalid_outfmt():
    invalid_params = valid_params.copy()
    invalid_params["outfmt"] = "invalid"
    with pytest.raises(ValueError, match="Invalid outfmt value"):
        BlastConfig(**invalid_params)


@pytest.mark.exception
def test_invalid_db_path():
    invalid_params = valid_params.copy()
    invalid_params["db_path"] = Path("invalid.fasta")
    with pytest.raises(
        FileNotFoundError, match="Invalid BLAST database file path"
    ):
        BlastConfig(**invalid_params)


@pytest.mark.exception
def test_query_path_and_subject_both_given():
    invalid_params = valid_params.copy()
    invalid_params["subject"] = Path("subject.fasta")
    with pytest.raises(
        ValueError, match="Only one of db_path or subject can be specified"
    ):
        BlastConfig(**invalid_params)


@pytest.mark.exception
def test_query_path_and_subject_none_given():
    invalid_params = valid_params.copy()
    invalid_params["db_path"] = None
    invalid_params["subject"] = None
    with pytest.raises(
        ValueError, match="Either db_path or subject must be specified"
    ):
        BlastConfig(**invalid_params)
