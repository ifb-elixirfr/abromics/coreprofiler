from invoke import task

package_name = "coreprofiler"


@task
def black_format(c):
    """Run black format."""
    c.run(f"black {package_name}/ --exclude migrations --line-length 79")


@task
def black_check(c):
    """Run black check."""
    c.run(f"black {package_name}/ --check --exclude migrations")


@task
def lint(c):
    """Run ruff."""
    c.run(f"ruff check --show-fixes --exclude .git,README.md,docs/ {package_name}/")


@task(lint)
def all(c):
    """Run all quality tests."""
    pass
